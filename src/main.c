#include <time.h>

#include "display.h"
#include "sound.h"
#include "util.h"

#define TITLE "anagrappler"

/* init or die */
void init(DisplayVars *dv, SoundVars *sv)
{
	memset(dv, 0, sizeof(DisplayVars));

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
		fatalf("SDL_Init: %s\n", SDL_GetError());

	init_physfs();
	init_sound(sv);
	init_display(dv, TITLE);
}

/* return 1 on quit, 0 otherwise */
int handle_event(SDL_Event event, SoundVars *sv)
{
	if (event.type == SDL_QUIT)
		return 1;
	if (event.type == SDL_KEYDOWN && !event.key.repeat) {
		switch (event.key.keysym.sym) {
		case SDLK_ESCAPE:
			queue_sound(sv, SFX_SFX);
			break;
		}
	}
	return 0;
}

/* main loop */
void event_loop(DisplayVars *dv, SoundVars *sv)
{
	int quit = 0, prev_ticks = 0;
	double dt;
	SDL_Event event;

	while (!quit) {
		dt = (double)(SDL_GetTicks() - prev_ticks) / 1000;
		prev_ticks = SDL_GetTicks();

		while (SDL_PollEvent(&event)) {
			quit |= handle_event(event, sv);
		}

		(void) dt; /* TODO */

		draw(dv);
	}
}

/* deinitialize data, ignoring errors */
void destroy(DisplayVars *dv, SoundVars *sv)
{
	deinit_display(dv);
	deinit_sound(sv);
	deinit_physfs();

	SDL_Quit();
}

/* program entry point */
int main(int argc, char *argv[])
{
	SoundVars sv;
	DisplayVars dv;

	(void) argc; /* avoid warning about unused parameter */
	argv0 = argv[0];

	srand(time(NULL));
	init(&dv, &sv);

	event_loop(&dv, &sv);

	destroy(&dv, &sv);
	
	return 0;
}
