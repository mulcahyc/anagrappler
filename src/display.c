#define WIDTH 320
#define HEIGHT 240
#define SCALE 2

#include <SDL2/SDL_image.h>

#include "display.h"
#include "util.h"

SDL_Texture *buffer, *font;

/* load a texture from a file */
SDL_Texture *load_texture(SDL_Renderer *renderer, char *path)
{
	SDL_RWops *rw = rw_from_physfs("font.png");
	SDL_Texture *texture = NULL;
	SDL_Surface *surface = IMG_Load_RW(rw, 1);
	if (!surface)
		fatalf("IMG_Load: %s: %s\n", path, IMG_GetError());
	texture = SDL_CreateTextureFromSurface(renderer, surface);
	if (!texture)
		fatalf("SDL_CreateTextureFromSurface: %s: %s\n", path,
				SDL_GetError());
	SDL_FreeSurface(surface);
	return texture;
}

/* draw a string of text with top-left coordinates x and y */
void draw_text(SDL_Renderer *renderer, char *string, int x, int y)
{
	int i;
	SDL_Rect srcrect = { 0, 0, 8, 8 }, dstrect;
	dstrect.x = x, dstrect.y = y, dstrect.w = dstrect.h = 8;
	
	for (i = 0; string[i] != '\0'; i++) {
		srcrect.x = (string[i] - 0x20) * 8;
		SDL_RenderCopy(renderer, font, &srcrect, &dstrect);
		dstrect.x += 8;
	}
}

/* initialize the display subsystem */
void init_display(DisplayVars *dv, char *title)
{
	dv->window = SDL_CreateWindow(title, SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED, WIDTH * SCALE, HEIGHT * SCALE,
			0);
	if (!dv->window)
		fatalf("SDL_CreateWindow: %s\n", SDL_GetError());

	dv->renderer = SDL_CreateRenderer(dv->window, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (!dv->renderer)
		fatalf("SDL_CreateRenderer: %s\n", SDL_GetError());
	SDL_SetRenderDrawColor(dv->renderer, 127, 127, 127, 255);
	SDL_RenderClear(dv->renderer);

	buffer = SDL_CreateTexture(dv->renderer, SDL_PIXELFORMAT_UNKNOWN,
			SDL_TEXTUREACCESS_TARGET, WIDTH / SCALE,
			HEIGHT / SCALE);
	if (!buffer)
		fatalf("SDL_CreateTexture: %s\n", SDL_GetError());

	if (!font)
		font = load_texture(dv->renderer, "font.png");
}

/* scale the back buffer to the size of the screen */
void scale_buffer(DisplayVars *dv)
{
	SDL_Rect srcrect = { 0, 0, WIDTH / SCALE, HEIGHT / SCALE },
		 dstrect = { 0, 0, WIDTH, HEIGHT };

	SDL_SetRenderTarget(dv->renderer, NULL);
	SDL_RenderCopy(dv->renderer, buffer, &srcrect, &dstrect);
}

/* draw the current scene to the screen */
void draw(DisplayVars *dv)
{
	SDL_SetRenderTarget(dv->renderer, buffer);
	SDL_SetRenderDrawColor(dv->renderer, 127, 127, 127, 255);
	SDL_RenderClear(dv->renderer);

	SDL_SetRenderDrawColor(dv->renderer, 255, 255, 255, 255);
	draw_text(dv->renderer, "HELLO, WORLD!", SCALE, SCALE);

	scale_buffer(dv);

	SDL_RenderPresent(dv->renderer);
}

/* clean up the display subsystem */
void deinit_display(DisplayVars *dv)
{
	if (dv->renderer)
		SDL_DestroyRenderer(dv->renderer);
	dv->renderer = NULL;
	if (dv->window)
		SDL_DestroyWindow(dv->window);
	dv->window = NULL;
	if (font)
		SDL_DestroyTexture(font);
	font = NULL;
}
