#include <SDL2/SDL.h>

char *argv0;

void fatalf(const char *, ...);
void init_physfs(void);
void deinit_physfs(void);
SDL_RWops *rw_from_physfs(char *);
