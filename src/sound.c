#include <dumb.h>
#include <physfs.h>

#include "sound.h"
#include "util.h"

#define AUDIO_BITS 16
#define AUDIO_CHANNELS 2
#define AUDIO_FREQ 48000
#define AUDIO_SAMPLES 4096
#define AUDIO_UNSIGNED 0

#define DUMB_FREQ 65536.0f
#define DEFAULT_BGM_VOLUME 1.0f

#define WAVE_DATA_OFFSET 44
#define NUM_SFX 1

Uint8 **sfx_array;

/* open function for physfs dfs */
void *open_physfs(const char *filename)
{
	PHYSFS_file *file = PHYSFS_openRead(filename);
	if (!file)
		fatalf("PHYSFS_openRead: %s: %s\n", filename,
				PHYSFS_getLastError());
	return file;
}

/* skip function for physfs dfs */
int skip_physfs(void *f, long n)
{
	PHYSFS_File *file = f;
	return !PHYSFS_seek(file, PHYSFS_tell(file) + n);
}

/* getc function for physfs dfs */
int getc_physfs(void *f)
{
	static int c;
	if (PHYSFS_read((PHYSFS_File *) f, &c, 1, 1) == -1)
		return -1;
	return c;
}

/* getnc function for physfs dfs */
long getnc_physfs(char *ptr, long n, void *f)
{
	return PHYSFS_read((PHYSFS_File *) f, ptr, 1, n);
}

/* close function for physfs dfs */
void close_physfs(void *f)
{
	PHYSFS_close((PHYSFS_File *) f);
}

/* dfs for reading from physfs */
DUMBFILE_SYSTEM physfs_dfs = {
	&open_physfs,
	&skip_physfs,
	&getc_physfs,
	&getnc_physfs,
	&close_physfs,
};

/* sdl audio callback */
void audio_callback(void *userdata, Uint8 *stream, int len)
{
	int i, j;
	SoundVars *sv = userdata;
	duh_render(sv->sigrenderer, AUDIO_BITS, AUDIO_UNSIGNED, sv->bgm_volume,
			DUMB_FREQ / sv->spec.freq,
			len / AUDIO_CHANNELS / AUDIO_BITS * 8, stream);
	for (i = 0; i < sv->sfx_pos; i++) {
		for (j = 0; j < len; j++) {
			stream[j] += sfx_array[sv->playing_sfx[i]->index][j];
		}
	}
}

/* allocates and reads a sound effect into the sfx array */
void load_sfx(int index, char *filename) {
	PHYSFS_sint64 len;
	PHYSFS_File *file = PHYSFS_openRead(filename);
	if (!file)
		fatalf("PHYSFS_openRead: %s: %s\n", filename,
				PHYSFS_getLastError());
	len = PHYSFS_fileLength(file);
	PHYSFS_seek(file, WAVE_DATA_OFFSET);
	sfx_array[index] = malloc(len - WAVE_DATA_OFFSET);
	PHYSFS_read(file, sfx_array[index], 1, len);
	PHYSFS_close(file);
}

/* queue a sound to play at the start of the next callback */
void queue_sound(SoundVars *sv, int index)
{
	PlayingSFX *sfx;
	if (sv->sfx_pos >= SFX_LEN)
		return;
	sfx = malloc(sizeof(PlayingSFX));
	sfx->index = index;
	sfx->offset = 0;
	sv->playing_sfx[sv->sfx_pos++] = sfx;
}

/* initialize audio subsystems */
void init_sound(SoundVars *sv)
{
	SDL_AudioSpec desired;

	memset(sv, 0, sizeof(SoundVars));

	/* open audio device */
	memset(&desired, 0, sizeof(SDL_AudioSpec));
	desired.freq = AUDIO_FREQ;
	desired.format = AUDIO_S16;
	desired.channels = AUDIO_CHANNELS;
	desired.samples = AUDIO_SAMPLES;
	desired.callback = &audio_callback;
	desired.userdata = sv;
	sv->dev = SDL_OpenAudioDevice(NULL, 0, &desired, &(sv->spec),
			SDL_AUDIO_ALLOW_FREQUENCY_CHANGE |
			SDL_AUDIO_ALLOW_CHANNELS_CHANGE);
	if (!sv->dev)
		fatalf("SDL_OpenAudioDevice: %s\n", SDL_GetError());

	/* init dumb */
	register_dumbfile_system(&physfs_dfs);
	sv->duh = dumb_load_it_quick("bgm.it");
	if (!sv->duh)
		fatalf("could not load resource: %s\n", "bgm.it");
	sv->sigrenderer = duh_start_sigrenderer(sv->duh, 0, AUDIO_CHANNELS, 0);
	sv->bgm_volume = DEFAULT_BGM_VOLUME;

	/* load sfx */
	sfx_array = calloc(NUM_SFX, sizeof(Uint8 **));
	load_sfx(0, "sfx.wav");

	SDL_PauseAudioDevice(sv->dev, 0);

}

/* deinitialize audio subsystems */
void deinit_sound(SoundVars *sv)
{
	int i;

	SDL_CloseAudioDevice(sv->dev);

	if (sv->sigrenderer)
		duh_end_sigrenderer(sv->sigrenderer);
	sv->sigrenderer = NULL;
	if (sv->duh)
		unload_duh(sv->duh);
	sv->duh = NULL;
	dumb_exit();

	for (i = 0; i < NUM_SFX; i++)
		free(sfx_array[i]);
	free(sfx_array);
	sfx_array = NULL;
}
