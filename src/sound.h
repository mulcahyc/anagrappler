#include <dumb.h>
#include <SDL2/SDL.h>

#define SFX_LEN 8

#define SFX_SFX 0

typedef struct {
	int index, offset;
} PlayingSFX;

typedef struct {
	SDL_AudioSpec spec;
	SDL_AudioDeviceID dev;
	DUH *duh;
	DUH_SIGRENDERER *sigrenderer;
	float bgm_volume;
	PlayingSFX *playing_sfx[SFX_LEN];
	int sfx_pos;
} SoundVars;

void init_sound(SoundVars *);
void queue_sound(SoundVars *, int index);
void deinit_sound(SoundVars *);
