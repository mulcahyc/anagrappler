#include <SDL2/SDL.h>

typedef struct {
	SDL_Window *window;
	SDL_Renderer *renderer;
} DisplayVars;

void init_display(DisplayVars *, char *);
void draw(DisplayVars *);
void deinit_display(DisplayVars *);
