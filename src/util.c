#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include <physfs.h>

#include "util.h"

#define ARCHIVE "data.zip"

/* print error and exit */
void fatalf(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	fprintf(stderr, "%s: ", argv0);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	exit(1);
}

/* initialize physfs */
void init_physfs(void)
{
	if (!PHYSFS_init(argv0))
		fatalf("PHYSFS_init: %s\n", PHYSFS_getLastError());
	if (!PHYSFS_mount(ARCHIVE, NULL, 0))
		fatalf("PHYSFS_mount: %s: %s\n", ARCHIVE,
				PHYSFS_getLastError());
}

/* deinitialize physfs */
void deinit_physfs(void)
{
	PHYSFS_deinit();
}

/* read a SDL_RWops from physfs */
SDL_RWops *rw_from_physfs(char *path)
{
	PHYSFS_file *file = PHYSFS_openRead(path);
	PHYSFS_sint64 len;
	char *buf;

	if (!file)
		fatalf("PHYSFS_openRead: %s: %s\n", path,
				PHYSFS_getLastError());

	len = PHYSFS_fileLength(file);
	buf = malloc(len);
	PHYSFS_read(file, buf, 1, len);
	PHYSFS_close(file);
	return SDL_RWFromMem(buf, len);
}
