CC = cc
CFLAGS = -Wall -Wextra -Werror -pedantic -std=c89 -I=include
LFLAGS = -ldl -lm -lpthread
OBJS = src/display.c src/main.c src/sound.c src/util.c lib/libdumb.a \
	   lib/libphysfs.a lib/libSDL2.a lib/libSDL2_image.a lib/libz.a
OBJ_NAME = anagrappler

INSTALL_DIR = /usr/local

all: $(OBJS)
	$(CC) $(OBJS) $(CFLAGS) $(LFLAGS) -o $(OBJ_NAME)

clean:
	rm -f $(OBJ_NAME)

install: $(OBJS)
	install -Dm 755 $(OBJ_NAME) $(INSTALL_DIR)/bin

uninstall:
	rm -f $(INSTALL_DIR)/bin/$(OBJ_NAME)
