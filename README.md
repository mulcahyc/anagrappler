anagrappler
===========
it appears to be a computer game of some sort.

to compile, you'll need to populate a directory called 'libs' with the
following files:

- libSDL2.a
- libSDL2\_image.a
- libdumb.a
- libphysfs.a
- libz.a

and create a file called 'data.zip' containing the following files:

- bgm.it
- font.png
- sfx.wav

how you accomplish this is entirely up to you. then simply run `make` and hope
for the best.
